#include "src/socket.hpp"

#define NUM_GROUPS 10
#define NUM_NODES_PER_GROUP 10
#define CHANNEL_DELAY "15ms"
#define NUM_OF_MESSAGES 20

#define MIN_INTERVAL_MS 2500
#define MAX_INTERVAL_MS 3000

namespace test_metrics {
    int random_interval();

    void onFetchValidationError(const ndn::Data& data, const ndn::security::v2::ValidationError& error);

    void onFetchTimeout(const ndn::Interest& interest);
}