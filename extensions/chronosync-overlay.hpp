#include "src/socket.hpp"

#include <ndn-cxx/face.hpp>

#include <boost/random.hpp>
#include <boost/thread/mutex.hpp>

namespace ndn {

class ChronoSyncOverlay
{
public:
  ChronoSyncOverlay(const bool isBottom, const Name upperLevelPrefix, const Name lowerLevelPrefix, const Name userPrefix, const Name routingPrefix, const int numberMessages);

  void initializeSync();

  void processSyncUpdate(const std::vector<chronosync::MissingDataInfo>& updates, const bool isLower);

  void handleDataUpper(const Data& data);
  void handleDataLower(const Data& data);

  void printData(const Data& data, bool fromLower);
  void delayedInterest(int id);

  void run();

 private:
  boost::asio::io_service m_ioService;
  Face m_face;
  Scheduler m_scheduler;

  shared_ptr<chronosync::Socket> m_socketUpper;
  shared_ptr<chronosync::Socket> m_socketLower;

  bool m_isBottom;

  Name m_syncPrefixUpper;
  Name m_syncPrefixLower;
  Name m_userPrefix;
  Name m_routingPrefix;

  int m_numberMessages;
  int m_numberMessagesReceived;

  void reportTime();

  boost::mutex mtx;
};

} // namespace ndn
