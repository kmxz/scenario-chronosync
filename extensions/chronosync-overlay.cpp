#include "test_metrics.hpp"
#include "chronosync-overlay.hpp"

namespace ndn {

  ChronoSyncOverlay::ChronoSyncOverlay(
    const bool isBottom,
    const Name upperLevelPrefix,
    const Name lowerLevelPrefix,
    const Name userPrefix,
    const Name routingPrefix,
    const int numberMessages
  ):
    m_face(m_ioService),
    m_scheduler(m_ioService),
    m_isBottom(isBottom),
    m_syncPrefixUpper(upperLevelPrefix),
    m_syncPrefixLower(lowerLevelPrefix),
    m_userPrefix(userPrefix),
    m_routingPrefix(routingPrefix),
    m_numberMessages(numberMessages),
    m_numberMessagesReceived(0) {}

  void ChronoSyncOverlay::initializeSync() {
    reportTime();
    std::cout << "Peer " << m_userPrefix << ": ChronoSync Instance Initialized with " << m_numberMessages << " pending messages: ";
    std::cout << "upperPrefix " << m_syncPrefixUpper << "; lowerPrefix " << m_syncPrefixLower << "; isBottom = " << (m_isBottom ? "true": "false") << "\n";

    Name routableUserPrefixUpper;
    routableUserPrefixUpper.clear();
    routableUserPrefixUpper.append(m_routingPrefix).append(m_userPrefix).append("upper");
    m_socketUpper = std::make_shared<chronosync::Socket>(m_syncPrefixUpper, routableUserPrefixUpper, m_face, bind(&ChronoSyncOverlay::processSyncUpdate, this, _1, false));
    if (!m_isBottom) {
      Name routableUserPrefixLower;
      routableUserPrefixLower.clear();
      routableUserPrefixLower.append(m_routingPrefix).append(m_userPrefix).append("lower");

      m_socketLower = std::make_shared<chronosync::Socket>(m_syncPrefixLower, routableUserPrefixLower, m_face, bind(&ChronoSyncOverlay::processSyncUpdate, this, _1, true));
    }
  }

  void ChronoSyncOverlay::reportTime() {
    std::cout << ns3::Simulator::Now().ToDouble(ns3::Time::S) << " s:\t";
  }

  void ChronoSyncOverlay::run() {
    m_scheduler.scheduleEvent(ndn::time::milliseconds(test_metrics::random_interval()), bind(&ChronoSyncOverlay::delayedInterest, this, 1));
  }

  void ChronoSyncOverlay::processSyncUpdate(const std::vector<chronosync::MissingDataInfo>& updates, bool isLower) {
    if (updates.empty()) {
      return;
    }
    auto handleEach = bind(isLower ? (&ChronoSyncOverlay::handleDataLower) : (&ChronoSyncOverlay::handleDataUpper), this, _1);
    for (unsigned int i = 0; i < updates.size(); i++) {
      for (chronosync::SeqNo seq = updates[i].low; seq <= updates[i].high; ++seq) {
        (isLower ? m_socketLower : m_socketUpper)->fetchData(updates[i].session, seq, handleEach, 1000);
      }
    }
  }

  void ChronoSyncOverlay::handleDataUpper(const Data& data) {
    m_numberMessagesReceived++;
    if (!m_isBottom) {
      m_socketLower->publishData(data.getContent().value(), data.getContent().value_size(), ndn::time::milliseconds(4000));
    }
    printData(data, false);
  }

  void ChronoSyncOverlay::handleDataLower(const Data& data) {
    m_numberMessagesReceived++;
    m_socketUpper->publishData(data.getContent().value(), data.getContent().value_size(), ndn::time::milliseconds(4000));
    printData(data, true);
  }

  void ChronoSyncOverlay::printData(const Data& data, bool fromLower) {
    Name::Component peerName = data.getName().at(1);

    std::string s(reinterpret_cast<const char*>(data.getContent().value()), data.getContent().value_size());

    reportTime();
    std::cout << "Peer " << m_userPrefix << ": Data received from " << peerName.toUri() << " : " <<  s << "; total messages received: " << m_numberMessagesReceived << "\n";
  }

  void ChronoSyncOverlay::delayedInterest(int id) {
    int s_rand = id * 10000 + test_metrics::random_interval();
    string message = "[From node " + m_userPrefix.toUri() + ": " + std::to_string(s_rand) + "]";
    reportTime();
    std::cout << "Peer " << m_userPrefix << ": Delayed Interest with id: " << s_rand << "\n";
    if (!m_isBottom) {
      m_socketLower->publishData(reinterpret_cast<const uint8_t *>(message.c_str()), message.size(), ndn::time::milliseconds(4000));
    }
    m_socketUpper->publishData(reinterpret_cast<const uint8_t*>(message.c_str()), message.size(), ndn::time::milliseconds(4000));

    if (id < m_numberMessages) {
      m_scheduler.scheduleEvent(ndn::time::milliseconds(test_metrics::random_interval()), bind(&ChronoSyncOverlay::delayedInterest, this, ++id));
    }
  }

} // namespace ndn
