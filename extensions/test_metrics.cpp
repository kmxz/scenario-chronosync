#include <boost/random.hpp>

#include "test_metrics.hpp"

namespace test_metrics {
    boost::mt19937 rng(20180601);

    boost::variate_generator<boost::mt19937&, boost::uniform_int<> > rangeUniformRandom(rng, boost::uniform_int<>(MIN_INTERVAL_MS, MAX_INTERVAL_MS));

    int random_interval() {
        return rangeUniformRandom();
    }

    void onFetchValidationError(const ndn::Data& data, const ndn::security::v2::ValidationError& error) {
        std::cout << "[ERROR] VALIDATION ERROR!" << std::endl;
    }

    void onFetchTimeout(const ndn::Interest& interest) {
      std::cout << "[ERROR] FETCH TIMEOUT: " <<  interest.toUri() << std::endl;
    }
}