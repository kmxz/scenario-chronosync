#include "ns3/ndnSIM-module.h"
#include "ns3/integer.h"
#include "ns3/string.h"
#include "ns3/boolean.h"
#include "ns3/double.h"

#include "chronosync-overlay.hpp"

namespace ns3 {
namespace ndn {

class ChronoSyncOverlayApp : public Application
{
public:
  static TypeId
  GetTypeId()
  {
    static TypeId tid = TypeId("ChronoSyncOverlayApp")
      .SetParent<Application>()
      .AddConstructor<ChronoSyncOverlayApp>()
      .AddAttribute("SyncPrefixUpper", "Sync Prefix (upper)", StringValue("/"),
                    MakeNameAccessor(&ChronoSyncOverlayApp::m_syncPrefixUpper), MakeNameChecker())
      .AddAttribute("SyncPrefixLower", "Sync Prefix (lower)", StringValue("/"),
                    MakeNameAccessor(&ChronoSyncOverlayApp::m_syncPrefixLower), MakeNameChecker())
      .AddAttribute("UserPrefix", "User Prefix", StringValue("/"),
                    MakeNameAccessor(&ChronoSyncOverlayApp::m_userPrefix), MakeNameChecker())
      .AddAttribute("RoutingPrefix", "Routing Prefix", StringValue("/"),
                    MakeNameAccessor(&ChronoSyncOverlayApp::m_routingPrefix), MakeNameChecker())
      .AddAttribute("NumberMessages", "Number of messages", IntegerValue(1),
                    MakeIntegerAccessor(&ChronoSyncOverlayApp::m_numberMessages), MakeIntegerChecker<int32_t>())
      .AddAttribute("IsBottom", "Is in bottom level", BooleanValue(false),
                    MakeBooleanAccessor(&ChronoSyncOverlayApp::m_isBottom), MakeBooleanChecker());
    return tid;
  }

protected:
  // inherited from Application base class.
  virtual void
  StartApplication()
  {
    m_instance.reset(new ::ndn::ChronoSyncOverlay(m_isBottom, m_syncPrefixUpper, m_syncPrefixLower, m_userPrefix, m_routingPrefix, m_numberMessages));
    m_instance->initializeSync();
    m_instance->run();
  }

  virtual void
  StopApplication()
  {
    m_instance.reset();
  }

private:
  std::unique_ptr<::ndn::ChronoSyncOverlay> m_instance;
  Name m_syncPrefixUpper;
  Name m_syncPrefixLower;
  Name m_userPrefix;
  Name m_routingPrefix;
  Name m_routableUserPrefix;
  int m_numberMessages;
  bool m_isBottom;
};

} // namespace ndn
} // namespace ns3
