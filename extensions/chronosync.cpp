/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015  Regents of the University of California.
 *
 * This file is part of ndnSIM. See AUTHORS for complete list of ndnSIM authors and
 * contributors.
 *
 * ndnSIM is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * ndnSIM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ndnSIM, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Spyridon (Spyros) Mastorakis <mastorakis@cs.ucla.edu>
 */

#include "test_metrics.hpp"
#include "chronosync.hpp"

namespace ndn {

ChronoSync::ChronoSync(const int numberMessages)
  : m_face(m_ioService)
  , m_scheduler(m_ioService)
  , m_numberMessages(numberMessages)
  , m_numberMessagesReceived(0)
{
}

void
ChronoSync::setSyncPrefix(const Name& syncPrefix)
{
  m_syncPrefix = syncPrefix;
}

void
ChronoSync::setUserPrefix(const Name& userPrefix)
{
  m_userPrefix = userPrefix;
}

void
ChronoSync::setRoutingPrefix(const Name& routingPrefix)
{
  m_routingPrefix = routingPrefix;
}

void
ChronoSync::delayedInterest(int id)
{
  int s_rand = id * 10000 + test_metrics::random_interval();
  string message = "[From node " + m_userPrefix.toUri() + ": " + std::to_string(s_rand) + "]";
  reportTime();
  std::cout << "Peer " << m_userPrefix << ": Delayed Interest with id: " << s_rand << "\n";
  m_socket->publishData(reinterpret_cast<const uint8_t*>(message.c_str()),
                        message.size(), ndn::time::milliseconds(4000));


  if (id < m_numberMessages)
    m_scheduler.scheduleEvent(ndn::time::milliseconds(test_metrics::random_interval()),
                              bind(&ChronoSync::delayedInterest, this, ++id));
}

void
ChronoSync::printData(const Data& data)
{
  Name::Component peerName = data.getName().at(1);

  std::string s(reinterpret_cast<const char*>(data.getContent().value()), data.getContent().value_size());

  m_numberMessagesReceived++;

  reportTime();
  std::cout << "Peer " << m_userPrefix << ": Data received from " << peerName.toUri() << " : " <<  s << "; total messages received: " << m_numberMessagesReceived << "\n";
}


void
ChronoSync::processSyncUpdate(const std::vector<chronosync::MissingDataInfo>& updates)
{
  if (updates.empty()) {
    return;
  }

  for (unsigned int i = 0; i < updates.size(); i++) {
    for (chronosync::SeqNo seq = updates[i].low; seq <= updates[i].high; ++seq) {
      m_socket->fetchData(updates[i].session, seq, bind(&ChronoSync::printData, this, _1), 1000);
    }
  }
}

void
ChronoSync::initializeSync()
{
  reportTime();
  std::cout << "Peer " << m_userPrefix << ": ChronoSync Instance Initialized with " << m_numberMessages << " pending messages\n";
  m_routableUserPrefix = Name();
  m_routableUserPrefix.clear();
  m_routableUserPrefix.append(m_routingPrefix).append(m_userPrefix);

  m_socket = std::make_shared<chronosync::Socket>(m_syncPrefix,
                                                  m_routableUserPrefix,
                                                  m_face,
                                                  bind(&ChronoSync::processSyncUpdate, this, _1));

}

void
ChronoSync::run()
{
  m_scheduler.scheduleEvent(ndn::time::milliseconds(test_metrics::random_interval()),
                            bind(&ChronoSync::delayedInterest, this, 1));
}

void
ChronoSync::reportTime()
{
  std::cout << ns3::Simulator::Now().ToDouble(ns3::Time::S) << " s:\t";
}

} // namespace ndn
