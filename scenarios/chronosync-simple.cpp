/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015  Regents of the University of California.
 *
 * This file is part of ndnSIM. See AUTHORS for complete list of ndnSIM authors and
 * contributors.
 *
 * ndnSIM is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * ndnSIM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * ndnSIM, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Spyridon (Spyros) Mastorakis <mastorakis@cs.ucla.edu>
 */

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/ndnSIM-module.h"

#include "../extensions/test_metrics.hpp"

namespace ns3 {
namespace ndn {

/**
 * This scenario simulates a very simple network topology:
 *
 * NUM_GROUPS * NUM_NODES_PER_GROUP nodes, with the "gateway" node of each group,
 * fully connected to each other, and they connect to all other nodes in that group
 *
 * Consumer requests data from producer with frequency 10 interests per second
 * (interests contain constantly increasing sequence number).
 *
 * For every received interest, producer replies with a data packet, containing
 * 1024 bytes of virtual payload.
 *
 * To run scenario and see what is happening, use the following command:
 *
 *     ./waf --run=chronosync-simple
 */

int
main(int argc, char *argv[])
{
  // setting default parameters for PointToPoint links and channels
  Config::SetDefault("ns3::PointToPointNetDevice::DataRate", StringValue("1Mbps"));
  Config::SetDefault("ns3::PointToPointChannel::Delay", StringValue(CHANNEL_DELAY));
  Config::SetDefault("ns3::QueueBase::MaxPackets", UintegerValue(20));

  // Read optional command-line parameters (e.g., enable visualizer with ./waf --run=<> --visualize
  CommandLine cmd;
  cmd.Parse(argc, argv);

  // Creating nodes
  NodeContainer nodes;
  nodes.Create(NUM_GROUPS * NUM_NODES_PER_GROUP);

  // Connecting nodes using two links
  PointToPointHelper p2p;
  for (int i = 0; i < NUM_GROUPS; i++) {
    int headOfThisGroup = i * NUM_NODES_PER_GROUP;
    for (int j = 1; j < NUM_NODES_PER_GROUP; j++) {
      p2p.Install(nodes.Get(headOfThisGroup), nodes.Get(headOfThisGroup + j));
    }
    for (int k = i + 1; k < NUM_GROUPS; k++) {
      p2p.Install(nodes.Get(headOfThisGroup), nodes.Get(k * NUM_NODES_PER_GROUP));
    }
  }

  // Install NDN stack on all nodes
  StackHelper ndnHelper;
  ndnHelper.SetDefaultRoutes(true);
  ndnHelper.InstallAll();

  // Choosing forwarding strategy
  ndn::StrategyChoiceHelper::InstallAll("/ndn", "ndn:/localhost/nfd/strategy/multicast");

  // Manually configure FIB routes
  for (int i = 0; i < NUM_GROUPS; i++) {
    int headOfThisGroup = i * NUM_NODES_PER_GROUP;
    for (int j = 1; j < NUM_NODES_PER_GROUP; j++) {
      ndn::FibHelper::AddRoute(nodes.Get(headOfThisGroup), "/ndn/broadcast/sync", nodes.Get(headOfThisGroup + j), 1);
      ndn::FibHelper::AddRoute(nodes.Get(headOfThisGroup + j), "/ndn/broadcast/sync", nodes.Get(headOfThisGroup), 1);
    }
    for (int k = i + 1; k < NUM_GROUPS; k++) {
      ndn::FibHelper::AddRoute(nodes.Get(headOfThisGroup), "/ndn/broadcast/sync", nodes.Get(k * NUM_NODES_PER_GROUP), 1);
      ndn::FibHelper::AddRoute(nodes.Get(k * NUM_NODES_PER_GROUP), "/ndn/broadcast/sync", nodes.Get(headOfThisGroup), 1);
    }
  }

  // Installing applications
  for (int i = 0; i < NUM_GROUPS; i++) {
    for (int j = 0; j < NUM_NODES_PER_GROUP; j++) {
      ndn::AppHelper peerI("ChronoSyncApp");
      peerI.SetAttribute("SyncPrefix", StringValue("/ndn/broadcast/sync"));
      peerI.SetAttribute("UserPrefix", StringValue("/peer" + std::to_string(i) + "-" + std::to_string(j)));
      peerI.SetAttribute("RoutingPrefix", StringValue("/ndn"));
      peerI.SetAttribute("NumberMessages", IntegerValue(NUM_OF_MESSAGES));
      peerI.Install(nodes.Get(i * NUM_NODES_PER_GROUP + j)).Start(Seconds(5));
    }
  }

  Simulator::Stop(Seconds(100.0));
  Simulator::Run();
  Simulator::Destroy();

  return 0;
}

} // namespace ndn
} // namespace ns3

int
main(int argc, char* argv[])
{
  return ns3::ndn::main(argc, argv);
}
